import { Component, ViewEncapsulation } from '@angular/core';
import { DragulaService } from 'ng2-dragula/ng2-dragula';
import {CdkDragDrop, moveItemInArray,transferArrayItem} from '@angular/cdk/drag-drop';
//import { EmailEditorComponent } from 'angular-email-editor';


@Component({
  templateUrl: 'draggable-cards.component.html',
  styleUrls: ['../../../../../node_modules/dragula/dist/dragula.min.css'],
  encapsulation: ViewEncapsulation.None
})



export class DraggableCardsComponent {

  colors = [ "Aqua","Black","Blue","BlueViolet","DarkKhaki","DarkSalmon","DeepSkyBlue"  ];
  dropEvent(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.colors, event.previousIndex, event.currentIndex);
  }


  items = [{name:'Carrots',dataObject:'1'},
    {name:'Tomatoes',dataObject:'2'},
    {name:'Onions',dataObject:'3'},
    {name:'Apples',dataObject:'4'},
    {name:'Avocados',dataObject:'5'},

  ];

  basket = [
    {name:'Oranges',dataObject:'6'},
    {name:'Bananas',dataObject:'7'},
    {name:'Cucumbers',dataObject:'8'}
  ];


  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
      console.log("1");
      console.log(this.items);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
      console.log("2");
      console.log(this.items);
      this.basket=[
        {name:'Oranges',dataObject:'6'},
        {name:'Bananas',dataObject:'7'},
        {name:'Cucumbers',dataObject:'8'}
      ];
    }
  }


  constructor(private dragulaService: DragulaService) {

    dragulaService.setOptions('second-bag', {
      moves: function (el, container, handle) {
        return handle.className === 'card-header drag';
      }
    });
    dragulaService.drag.subscribe((value) => {
      console.log("1");
      this.onDrag(value.slice(1));
    });
    dragulaService.drop.subscribe((value) => {
      console.log("2");
      this.onDrop(value.slice(1));
    });
    dragulaService.over.subscribe((value) => {
      console.log("3");
      this.onOver(value.slice(1));
    });
    dragulaService.out.subscribe((value) => {
      console.log("4");
      this.onOut(value.slice(1));
    });
  }


  private hasClass(el: any, name: string) {
    console.log("5");

    console.log(el);
    return new RegExp('(?:^|\\s+)' + name + '(?:\\s+|$)').test(el.className);
  }

  private addClass(el: any, name: string) {
    console.log("6");
    if (!this.hasClass(el, name)) {
      el.className = el.className ? [el.className, name].join(' ') : name;
    }
  }

  private removeClass(el: any, name: string) {
    console.log("7");
    if (this.hasClass(el, name)) {
      el.className = el.className.replace(new RegExp('(?:^|\\s+)' + name + '(?:\\s+|$)', 'g'), '');
    }
  }

  private onDrag(args) {
    console.log("8");
    const [e, el] = args;
    this.removeClass(e, 'ex-moved');
  }

  private onDrop(args) {
    console.log("9");
    const [e, el] = args;
    this.addClass(e, 'ex-moved');
  }

  private onOver(args) {
    console.log("10");
    const [e, el, container] = args;
    this.addClass(el, 'ex-over');
  }

  private onOut(args) {
    console.log("11");
    const [e, el, container] = args;
    this.removeClass(el, 'ex-over');
  }
}
