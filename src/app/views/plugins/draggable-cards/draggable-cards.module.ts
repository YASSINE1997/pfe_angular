
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DragulaModule } from 'ng2-dragula/ng2-dragula';
import { DragDropModule } from '@angular/cdk/drag-drop';

// Spinners
import { DraggableCardsComponent } from './draggable-cards.component';

// Routing
import { DraggableCardsRoutingModule } from './draggable-cards-routing.module';
//
@NgModule({
  imports: [
    DraggableCardsRoutingModule,
    CommonModule,
    DragulaModule,
    DragDropModule
  ],
  declarations: [
    DraggableCardsComponent
  ],
  bootstrap: [DraggableCardsComponent]
})
export class DraggableCardsModule { }
